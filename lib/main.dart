import 'package:flutter/material.dart';

void main() {
  runApp(MyFlutterApp());
}

class MyFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: "My Flutter App",
        home: new Scaffold(
            appBar: new AppBar(title: new Text("appbar")),
            body: new HomeWidget()));
  }
}

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: 20,
      itemBuilder: (context, rowNumber) {
        return new Container(
            padding: new EdgeInsets.all(16.0),
            child: new Column(children: <Widget>[
              new Image.network(
                  "https://archive.org/services/get-item-image.php?identifier=100ah-12v-lifepo4-deep-cycle-battery&mediatype=audio&collection=opensource_audio"),
              new Text("Archive item: ",
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18.0)),
              new Divider(color: Colors.green)
            ]));
      },
    );
  }
}
